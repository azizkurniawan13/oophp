<?php
require_once('animal.php');

class Frog extends Animal
{
    protected $jump;

    public function __construct($name, $leg, $cold_blooded, $jump)
    {
        parent::__construct($name, $leg, $cold_blooded);
        $this->jump = $jump;
    }

    public function getJump()
    {
        return $this->jump;
    }
}

$kodok = new Frog('buduk', 4, 'no', 'Hop Hop');
