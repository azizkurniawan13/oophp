<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Type of Animal</title>
</head>

<body>
    <h2>Type of Animal</h2>
    <br>
    <p>Name : <?= $sheep->getName(); ?></p>
    <p>Legs : <?= $sheep->getLeg(); ?></p>
    <p>Cold Blooded : <?= $sheep->get_cold_blooded(); ?></p>
    <br>
    <p>Name : <?= $kodok->getName(); ?></p>
    <p>Legs : <?= $kodok->getLeg(); ?></p>
    <p>Cold Blooded : <?= $kodok->get_cold_blooded(); ?></p>
    <p>Jump : <?= $kodok->getJump(); ?></p>
    <br>
    <p>Name : <?= $sungokong->getName(); ?></p>
    <p>Legs : <?= $sungokong->getLeg(); ?></p>
    <p>Cold Blooded : <?= $sungokong->get_cold_blooded(); ?></p>
    <p>Yell : <?= $sungokong->getYell(); ?></p>
</body>

</html>