<?php 
require_once('animal.php');

class Ape extends Animal {
    protected $yell;

    public function __construct($name, $leg, $cold_blooded, $yell)
    {
        parent::__construct($name, $leg, $cold_blooded);
        $this->yell = $yell;
    }

    public function getYell() {
        return $this->yell;
    }

}

$sungokong = new Ape('kera sakti', 2,'no', 'Auoo');
?>