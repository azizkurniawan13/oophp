<?php
class Animal
{
    protected $name;
    protected $leg;
    protected $cold_blooded;

    public function __construct($name, $leg, $cold_blooded)
    {
        $this->name = $name;
        $this->leg = $leg;
        $this->cold_blooded = $cold_blooded;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getLeg()
    {
        return $this->leg;
    }

    public function get_cold_blooded()
    {
        return $this->cold_blooded;
    }
}

$sheep = new Animal('Shaun', 4, 'no');
